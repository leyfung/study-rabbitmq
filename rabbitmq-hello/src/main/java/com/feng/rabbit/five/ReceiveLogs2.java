package com.feng.rabbit.five;

import com.feng.rabbit.util.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.nio.charset.StandardCharsets;

/**
 * @author jianglifeng
 * @Description 消息的接收
 * @createTime 2021年07月18日
 */
public class ReceiveLogs2 {
    //交换机的名称
    public static final String EXCHANGE_NAME = "logs";
    public static void main(String[] args) throws Exception {
        Channel channel = RabbitMqUtils.getChannel();
        //声明一个交换机
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        //声明一个队列 临时队列
        /*
        生成一个临时队列，队列名是随机的，当消费者断开与队列的连接时，队列自动删除
         */
        String queue = channel.queueDeclare().getQueue();
        /*
        绑定交换机与队列
         */
        channel.queueBind(queue,EXCHANGE_NAME,"");
        System.out.println("等待接收消息，把接收到的消息打印在屏幕上。。。");
        //接收消息
        DeliverCallback deliverCallback = (consumerTag,message) -> {
            System.out.println("ReceiveLogs2控制台打印接收到的消息："+new String(message.getBody(), StandardCharsets.UTF_8));
        };
        channel.basicConsume(queue,true,deliverCallback,consumerTag -> {});
    }
}
