package com.feng.rabbit.util;

/**
 * @author jianglifeng
 * @Description TODO 睡眠工具类
 * @createTime 2021年07月14日
 */
public class SleepUtils {
    public static void sleep(int second) {
        try {
            Thread.sleep(1000 * second);
        } catch (InterruptedException _ignored) {
            Thread.currentThread().interrupt();
        }
    }
}
