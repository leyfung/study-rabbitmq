package com.feng.rabbit.one;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * @author jianglifeng
 * @Description TODO
 * @createTime 2021年06月27日
 */
public class Producer {
    //队列名称
    public static final String QUEUE_NAME = "hello";

    //发消息
    public static void main(String[] args) throws IOException, TimeoutException {
        //创建一个连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        //工厂ip连接Rabbit MQ的队列
        factory.setHost("127.0.0.1");
        //用户名
        factory.setUsername("admin");
        //密码
        factory.setPassword("123");
        //创建连接
        Connection connection = factory.newConnection();
        //创建信道
        Channel channel = connection.createChannel();
        /*
        生成一个队列
        1、队列名称
        2、队列里面的消息是否持久化，默认存在内存
        3、该队列是否使用只供多个消费者进行消费，是否进行消息共享
        4、是否自动删除，最后一个消费者断开连接，队列是佛自动删除
        5、其他参数：
         */
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        //发消息
        String message = "hello world";
        /*
        1、发送到哪一个交换机
        2、路由的key，也就是队列名称
        3、其他参数信息
        4、消息体
         */
        channel.basicPublish("",QUEUE_NAME,null,message.getBytes(StandardCharsets.UTF_8));
        System.out.println("消息发送完毕");
    }
}
